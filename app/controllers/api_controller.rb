class ApiController < ApplicationController
  skip_before_action :authenticate_user!
  skip_before_action :verify_authenticity_token

  def about

    render json: {
      time: Time.now.strftime("%H:%M"),
      date: Time.now.strftime("%Y-%m-%d"),
      name: 'Andres Soop',
      fruits: ['banana', 'mango', 'kiwi', 'tomato', 'pineapple']
    }

  end

  def haiku

    render json: {
      haiku: reverse_haiku,
      name: uppercase_name
    }

  end

  def haiku_params
    params.permit(:haiku, :name)
  end

  def reverse_haiku 
    haiku_params[:haiku].split(' ').reverse.join(' ')
  end

  def uppercase_name 
    haiku_params[:name].upcase
  end

end
