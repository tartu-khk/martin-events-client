# martin-events-client

This is a README and is normally a document or whatever steps to get the application up and running

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. See deployment for notes on how to deploy the project on a live system.


### Prerequisites

You'll need:

*a computer*

### Installing

A step by step series of examples that tell you how to get a development up and running

Cloning git repo:
```
git clone https://github.com/andressoop/martin-events-client
```
Opening the repo root folder:
```
cd martin-events-client
```
Important install (dont' ask why):
```
bundle install
```
Making database:
```
rake db:migrate
```
Import seed data:
```
rake db:seed
```
Create secret.env into root folder and include:
```
export SENDGRID_API_KEY='YOUR KEY'
export RECAPTCHA_SITE_KEY='YOUR KEY'
export RECAPTCHA_SECRET_KEY='YOUR KEY'
```
How to get your Sendgrid API key:
```
Go to https://sendgrid.com sign up or log in and create a new API key
```
How to get your Recaptcha API key:
```
Go to https://www.google.com/recaptcha/admin and generate new keys. 
Note: use localhost or 127.0.0.1 in domain if using localhost:3000
```
Replace YOUR KEY with the generated API keys in secret.env

Import secrets:
```
source secret.env
```
Starting rails server to see your result in a browser:
```
rails s
```

## Built With

* [Ruby](https://www.ruby-lang.org/en/) - Version 2.5.1p57
* [Rails](https://rubyonrails.org/) - Version 5.2.1


## License

This project is licensed under the MIT License

